using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HWO.MessageData
{
  public class JoinRaceData
  {
    [JsonProperty("botId")]
    public BotIdData BotId;
    [JsonProperty("trackName")]
    public string TrackName;
    [JsonProperty("password")]
    public string Password;
    [JsonProperty("carCount")]
    public int? CarCount;
  }

  public class BotIdData
  {
    [JsonProperty("name")]
    public string Name;
    [JsonProperty("key")]
    public string Key;
  }

  public class CarIdData
  {
    [JsonProperty("name")]
    public string Name;
    [JsonProperty("color")]
    public string Color;
  }

  public class GameInitData
  {
    [JsonProperty("race")]
    public RaceData Race;
  }

  public class RaceData
  {
    [JsonProperty("track")]
    public TrackData Track;
    [JsonProperty("cars")]
    public List<CarData> Cars;
    [JsonProperty("raceSession")]
    public RaceSessionData RaceSession;
  }

  public class TrackData
  {
    [JsonProperty("id")]
    public string Id;
    [JsonProperty("name")]
    public string Name;
    [JsonProperty("pieces")]
    public List<TrackPieceData> Pieces;
    [JsonProperty("lanes")]
    public List<LaneData> Lanes;
    [JsonProperty("startingPoint")]
    public StartingPointData StartingPoint;
  }

  public class TrackPieceData
  {
    [JsonProperty("length")]
    public double Length = 0;
    [JsonProperty("switch")]
    public bool Switch = false;
    [JsonProperty("radius")]
    public double Radius = 0;
    [JsonProperty("angle")]
    public double Angle = 0;

    public double[] TotalLength;

    public int PreferredEndLane = 0;

    public double TargetVelocityWhileEvaluating = 0;
    public bool SwitchEvaluated = false;
    public SwitchLaneData SwitchRequest = SwitchLaneData.NoChange;
  }

  public class LaneData
  {
    [JsonProperty("distanceFromCenter")]
    public double DistanceFromCenter;
    [JsonProperty("index")]
    public int Index;
  }

  public class StartingPointData
  {
    [JsonProperty("position")]
    public PositionData Position;
    [JsonProperty("angle")]
    public double Angle;
  }

  public class PositionData
  {
    [JsonProperty("x")]
    public double X;
    [JsonProperty("y")]
    public double Y;
  }

  public class CarData
  {
    [JsonProperty("id")]
    public CarIdData Id;
    [JsonProperty("dimensions")]
    public CarDimensionsData Dimensions;
  }

  public class CarDimensionsData
  {
    [JsonProperty("length")]
    double Length;
    [JsonProperty("width")]
    double Width;
    [JsonProperty("guideFlagPosition")]
    double GuideFlagPosition;
  }

  public class RaceSessionData
  {
    [JsonProperty("laps")]
    public int Laps;
    [JsonProperty("maxLapTimeMs")]
    public double MaxLapTimeMs;
    [JsonProperty("quickRace")]
    public bool QuickRace;
    [JsonProperty("dirationMs")]
    public int DurationMs;
    /// <summary>
    /// In qualifying race there's only DurationMs attribute and others are empty and vice versa
    /// </summary>
    public bool IsQualifyingRace
    {
      get
      {
        return (this.DurationMs > 0);
      }
    }
  }

  public class CarPositionData
  {
    [JsonProperty("id")]
    public CarIdData Id;
    [JsonProperty("angle")]
    public double Angle;
    [JsonProperty("piecePosition")]
    public PiecePositionData PiecePosition;
  }

  public class PiecePositionData
  {
    [JsonProperty("pieceIndex")]
    public int PieceIndex;
    [JsonProperty("inPieceDistance")]
    public double InPieceDistance;
    [JsonProperty("lane")]
    public PieceLaneData Lane = new PieceLaneData();
    [JsonProperty("lap")]
    public int Lap;
  }

  public class PieceLaneData
  {
    [JsonProperty("startLaneIndex")]
    public int StartLaneIndex;
    [JsonProperty("endLaneIndex")]
    public int EndLaneIndex;
  }

  public class DnfData
  {
    [JsonProperty("car")]
    public CarIdData Car;
    [JsonProperty("reason")]
    public string Reason;
  }

  public enum SwitchLaneData
  {
    Left,
    Right,
    NoChange
  }

  public class TurboAvailableData
  {
    [JsonProperty("turboDurationMilliseconds")]
    public double TurboDurationMilliseconds;
    [JsonProperty("turboDurationTicks")]
    public int TurboDurationTicks;
    [JsonProperty("turboFactor")]
    public double TurboFactor;
  }
}
