using System;
using System.Collections.Generic;
using System.Text;
using HWO.MessageData;
using Newtonsoft.Json;

namespace HWO.Messages
{
  /// <summary>
  /// Basic non-typed message wrapped
  /// </summary>
  public class MessageWrapper
  {
    [JsonProperty("msgType")]
    public string MsgType;
    [JsonProperty("data")]
    public Object Data;
  }

  /// <summary>
  /// Base class for BOT messages
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class Message<T>
  {
    public Message()
    {
    }

    public Message(string msgType)
    {
      this.MsgType = msgType;
    }

    [JsonProperty("msgType")]
    public string MsgType;
    [JsonProperty("data")]
    public T Data;

    public string ToJson()
    {
      return JsonConvert.SerializeObject(this);
    }
  }

  /// <summary>
  /// 
  /// </summary>
  public class PingClientMessage : Message<object>
  {
    public PingClientMessage(int gameTick)
      : base("ping")
    {
      this.GameTick = gameTick;
    }
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class JoinClientMessage : Message<BotIdData>
  {
    public JoinClientMessage(string name, string key)
      : base("join")
    {
      this.Data = new BotIdData() { Name = name, Key = key };
    }
  }

  /// <summary>
  /// 
  /// </summary>
  public class CreateRaceClientMessage : Message<JoinRaceData>
  {
    public CreateRaceClientMessage(string name, string key, string trackName, string password, int carCount)
      : base("createRace")
    {
      this.Data = new JoinRaceData()
      {
        BotId = new BotIdData() { Name = name, Key = key },
        TrackName = trackName,
        Password = password,
        CarCount = carCount
      };
    }
  }

  /// <summary>
  /// 
  /// </summary>
  public class JoinRaceClientMessage : Message<JoinRaceData>
  {
    public JoinRaceClientMessage(string name, string key, string trackName, string password, int? carCount)
      : base("joinRace")
    {
      this.Data = new JoinRaceData() {
        BotId = new BotIdData() { Name = name, Key = key },
        TrackName = trackName,
        Password = password,
        CarCount = carCount };
    }
  }

  /// <summary>
  /// 
  /// </summary>
  public class ThrottleClientMessage : Message<double>
  {
    public ThrottleClientMessage(double throttle, int gameTick)
      : base("throttle")
    {
      this.Data = throttle;
      this.GameTick = gameTick;
    }
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class SwitchLaneClientMessage : Message<string>
  {
    public SwitchLaneClientMessage(SwitchLaneData direction, int gameTick)
      : base("switchLane")
    {
      if (direction == SwitchLaneData.Left)
        this.Data = "Left";
      else if (direction == SwitchLaneData.Right)
        this.Data = "Right";
      this.GameTick = gameTick;
    }
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class TurboClientMessage : Message<string>
  {
    public TurboClientMessage(string customMessage, int gameTick)
      : base("turbo")
    {
      this.Data = customMessage;
      this.GameTick = gameTick;
    }
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class YourCarServerMessage : Message<CarIdData>
  {
  }

  /// <summary>
  /// 
  /// </summary>
  public class GameInitServerMessage : Message<GameInitData>
  {
  }

  /// <summary>
  /// 
  /// </summary>
  public class GameStartServerMessage : Message<object>
  {
  }

  /// <summary>
  /// 
  /// </summary>
  public class CarPositionsServerMessage : Message<List<CarPositionData>>
  {
    [JsonProperty("gameId")]
    public string GameId;
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class CrashServerMessage : Message<CarIdData>
  {
    [JsonProperty("gameId")]
    public string GameId;
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class SpawnServerMessage : Message<CarIdData>
  {
    [JsonProperty("gameId")]
    public string GameId;
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class DnfServerMessage : Message<DnfData>
  {
    [JsonProperty("gameId")]
    public string GameId;
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class FinishServerMessage : Message<CarIdData>
  {
    [JsonProperty("gameId")]
    public string GameId;
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class TurboAvailableServerMessage : Message<TurboAvailableData>
  {
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class TurboStartServerMessage : Message<CarIdData>
  {
    [JsonProperty("gameTick")]
    public int GameTick;
  }

  /// <summary>
  /// 
  /// </summary>
  public class TurboEndServerMessage : Message<CarIdData>
  {
    [JsonProperty("gameTick")]
    public int GameTick;
  }

}
