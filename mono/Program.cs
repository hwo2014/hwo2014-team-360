using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using HWO;
using HWO.MessageData;
using HWO.Messages;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Program
{
  public static void Main(string[] args)
  {
    string host = args[0];
    int port = int.Parse(args[1]);
    string botName = args[2];
    string botKey = args[3];

    Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

    string x = JsonConvert.SerializeObject(new JoinClientMessage(botName, botKey));
    JoinClientMessage y = JsonConvert.DeserializeObject<JoinClientMessage>(x);

    using (TcpClient client = new TcpClient(host, port))
    {
      NetworkStream stream = client.GetStream();
      StreamReader reader = new StreamReader(stream);
      StreamWriter writer = new StreamWriter(stream);
      writer.AutoFlush = true;

      // create bot agent
      BotAgent bot = new BotAgent(reader, writer, botName, botKey);

      // agent main loop
      bot.MainLoop();

    }
  }
}



