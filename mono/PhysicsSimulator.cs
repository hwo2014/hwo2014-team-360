using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using HWO.MessageData;

namespace HWO
{
  public class GameTickPhysicsData
  {
    public int GameTick;
    public PiecePositionData PiecePosition = new PiecePositionData();
    public double Angle;
    public double Velocity;
    public double Radius;
    public double Throttle;
  }

  public class ModelConstants
  {
    public double A;
    public double B;
    public double k;
    public double K;
    public double F;
    public double f;
    public double MaxSlipAngle = 58;

    public double AccelerationModelSSE = Double.PositiveInfinity;
    public bool IsSlipModelComputed = false;
    public bool IsAccelerationModelComputed = false;
  }

  /// <summary>
  /// 
  /// </summary>
  public class PhysicsSimulator
  {
    private string _myCarName;
    
    /// <summary>
    /// 
    /// </summary>
    public Dictionary<string, List<GameTickPhysicsData>> Data { get; private set; }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="myCarName"></param>
    public PhysicsSimulator(string myCarName)
    {
      _myCarName = myCarName;
      this.Data = new Dictionary<string, List<GameTickPhysicsData>>();

      this.ModelConstants = new ModelConstants();
    }

    /// <summary>
    /// Track data
    /// </summary>
    public TrackData Track { get; private set; }

    /// <summary>
    /// Piece index of the start of the longest straight part
    /// </summary>
    public int LongestStraightStartIndex = -1;

    /// <summary>
    /// 
    /// </summary>
    public int SecondLongestStraightStartIndex = -1;

    /// <summary>
    /// 
    /// </summary>
    public ModelConstants ModelConstants { get; private set; }

    /// <summary>
    /// 
    /// </summary>
    public bool IsQualifyingRace { get; private set; }

    /// <summary>
    /// 
    /// </summary>
    public List<CarData> Cars { get; private set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public GameTickPhysicsData GetLastTickData(string name)
    {
      if (this.Data[name].Count > 0)
        return this.Data[name][this.Data[name].Count - 1];
      else
        return null;
    }

    /// <summary>
    /// Game init can be called twice - first for qualifying round, second time for actual race
    /// </summary>
    /// <param name="gameInit"></param>
    public void GameInit(GameInitData gameInit)
    {
      // save track information
      if (this.Track == null)
      {
        this.Track = gameInit.Race.Track;
        this.IsQualifyingRace = gameInit.Race.RaceSession.IsQualifyingRace;
        this.Cars = gameInit.Race.Cars;

        // Calculate total length for all pieces and all lanes in map
        _calculateTotalPieceLengths();

        // Calculate start of the longest straight
        this.LongestStraightStartIndex = -1;
        this.SecondLongestStraightStartIndex = -1;
        int longestLength = 0;
        int secondLongestLength = 0;
        for (int i = 0; i < Track.Pieces.Count; i++)
        {
          if (Track.Pieces[i].Radius == 0)
          {
            int len = 0;
            for (int j = 0; j < Track.Pieces.Count; j++)
            {
              if (Track.Pieces[(i + j) % Track.Pieces.Count].Radius != 0) break;
              len++;
            }
            if (len > longestLength)
            {
              secondLongestLength = longestLength;
              longestLength = len;
              this.SecondLongestStraightStartIndex = this.LongestStraightStartIndex;
              this.LongestStraightStartIndex = i;
            }
            else if (len > secondLongestLength)
            {
              secondLongestLength = len;
              this.SecondLongestStraightStartIndex = i;
            }
          }
        }

        // move by one to back
        this.LongestStraightStartIndex = (this.LongestStraightStartIndex - 1 + Track.Pieces.Count) % Track.Pieces.Count;
        this.SecondLongestStraightStartIndex = (this.SecondLongestStraightStartIndex - 1 + Track.Pieces.Count) % Track.Pieces.Count;

        // filter out short length
        if (secondLongestLength < 3.0 * (double)longestLength / 4.0) this.SecondLongestStraightStartIndex = -1;

      }
    }


    public void CalculateTargetConstantVelocitiesWhileEvaluatingSlipModel(int lane)
    {
      // Calculate target constant velocities at curves while evaluating slip model
      double targetCentripedalAcc = 0.1;
      for (int i = 0; i < this.Track.Pieces.Count; i++)
      {
        TrackPieceData piece = this.Track.Pieces[i];
        // piece radius
        double radius = piece.Radius;
        // skip straights
        if (radius == 0) continue;
        // radius @ lane
        if (piece.Angle < 0)
          radius = -radius - this.Track.Lanes[lane].DistanceFromCenter;
        else
          radius = radius - this.Track.Lanes[lane].DistanceFromCenter;
        // compute target velocity @ curve based on centripedal acc
        piece.TargetVelocityWhileEvaluating = Math.Sqrt(targetCentripedalAcc * Math.Abs(radius));
        // increase target centripedal acc
        targetCentripedalAcc += 0.02;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public GameTickPhysicsData MyLastTickData
    {
      get
      {
        if (Data[_myCarName].Count < 1) return null;
        return Data[_myCarName][Data[_myCarName].Count - 1];
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public GameTickPhysicsData MyPreviousTickData
    {
      get
      {
        if (Data[_myCarName].Count < 2) return null;
        return Data[_myCarName][Data[_myCarName].Count - 2];
      }
    }

    /// <summary>
    /// Calculate engine power and air drag constants
    /// </summary>
    /// <param name="list"></param>
    /// <param name="i"></param>
    private void _computeConstantsFf(List<GameTickPhysicsData> x, int i)
    {
      double a = x[i - 0].Velocity;
      double b = x[i - 1].Velocity;
      double c = x[i - 2].Velocity;
      
      double t = x[i - 1].Throttle;
      double u = x[i - 2].Throttle;

      double F = (b * b - a * c) / (b * u - c * t);
      double f = (a * u - b * (t + u) + c * t) / (c * t - b * u);

      this.ModelConstants.F = F;
      this.ModelConstants.f = f;
      this.ModelConstants.IsAccelerationModelComputed = true;

      Console.WriteLine(String.Format("F={0}\tf={1}", F, f));
      Console.WriteLine(String.Format("Vmax={0}", F / f));
    }

    private void _computeSadoMaso3(List<GameTickPhysicsData> X, int i)
    {
      double a = Math.Abs(X[i - 0].Angle);
      double b = Math.Abs(X[i - 1].Angle);
      double c = Math.Abs(X[i - 2].Angle);
      double d = Math.Abs(X[i - 3].Angle);
      double e = Math.Abs(X[i - 4].Angle);
      double f = Math.Abs(X[i - 5].Angle);

      double q = X[i - 1].Velocity;
      double r = X[i - 2].Velocity;
      double s = X[i - 3].Velocity;
      double t = X[i - 4].Velocity;

      double v = Math.Sqrt(Math.Abs(X[i - 1].Radius));
      double w = Math.Sqrt(Math.Abs(X[i - 2].Radius));
      double x = Math.Sqrt(Math.Abs(X[i - 3].Radius));
      double y = Math.Sqrt(Math.Abs(X[i - 4].Radius));

      double A = -((((-(a * q * r * r * v - b * q * q * r * w) * (q * s * s * v - q * q * s * x) + (q * r * r * v - q * q * r * w) * (a * q * s * s * v - c * q * q * s * x))
        * ((q * r * r * v - q * q * r * w) * (t * t * (a * v - 2 * b * v + c * v) - q * q * (d * y - 2 * e * y + f * y)) - (r * r * (a * v - 2 * b * v + c * v) - q * q * (b * w - 2 * c * w + d * w)) * (q * t * t * v - q * q * t * y))
        - ((q * r * r * v - q * q * r * w) * (s * s * (a * v - 2 * b * v + c * v) - q * q * (c * x - 2 * d * x + e * x)) - (r * r * (a * v - 2 * b * v + c * v) - q * q * (b * w - 2 * c * w + d * w)) * (q * s * s * v - q * q * s * x))
        * (-(a * q * r * r * v - b * q * q * r * w) * (q * t * t * v - q * q * t * y) + (q * r * r * v - q * q * r * w) * (a * q * t * t * v - d * q * q * t * y))))
        / ((-(a * q * r * r * v - b * q * q * r * w) * (q * s * s * v - q * q * s * x) + (q * r * r * v - q * q * r * w) * (a * q * s * s * v - c * q * q * s * x))
        * ((q * r * r * v - q * q * r * w) * (t * t * (a * v + b * v) - q * q * (d * y + e * y)) - (r * r * (a * v + b * v) - q * q * (b * w + c * w))
        * (q * t * t * v - q * q * t * y)) - ((q * r * r * v - q * q * r * w) * (s * s * (a * v + b * v) - q * q * (c * x + d * x)) - (r * r * (a * v + b * v) - q * q * (b * w + c * w))
        * (q * s * s * v - q * q * s * x)) * (-(a * q * r * r * v - b * q * q * r * w) * (q * t * t * v - q * q * t * y) + (q * r * r * v - q * q * r * w) * (a * q * t * t * v - d * q * q * t * y))));

      double B = -((((q * r * r * v - q * q * r * w) * (s * s * (a * v + a * A * v - 2 * b * v + A * b * v + c * v) - q * q * (c * x + A * c * x - 2 * d * x + A * d * x + e * x))
        - (r * r * (a * v + a * A * v - 2 * b * v + A * b * v + c * v)
        - q * q * (b * w + A * b * w - 2 * c * w + A * c * w + d * w))
        * (q * s * s * v - q * q * s * x)))
        / (-(a * q * r * r * v - b * q * q * r * w) * (q * s * s * v - q * q * s * x) + (q * r * r * v - q * q * r * w) * (a * q * s * s * v - c * q * q * s * x)));

      double k = ((b * q * v * w + A * b * q * v * w - 2 * c * q * v * w + A * c * q * v * w + d * q * v * w - a * r * v * w - a * A * r * v * w + 2 * b * r * v * w - A * b * r * v * w - c * r * v * w - a * B * q * r * v * w + b * B * q * r * v * w)) / (q * (r * r * v - q * r * w));

      double K = ((1 + A + B * q) * (-a + ((2 - A) * b) / (1 + A + B * q) - (c) / (1 + A + B * q) + (k * q * q) / ((1 + A + B * q) * v))) / q;



      this.ModelConstants.A = (double)A;
      this.ModelConstants.B = (double)B;
      this.ModelConstants.k = (double)k;
      this.ModelConstants.K = (double)K;
      this.ModelConstants.IsSlipModelComputed = true;

      Console.WriteLine(String.Format("A={0}\tB={1}", A, B));
      Console.WriteLine(String.Format("K={0}\tk={1}", K, k));
      Console.WriteLine("----------------------------");
    }


    private bool _isComputingSlipModelConstants = false;
    private class DataForCalculation
    {
      public List<GameTickPhysicsData> X;
      public int I;
    }

    /// <summary>
    /// Compute A,B constants in curve at the start of the drifting
    /// </summary>
    /// <param name="x"></param>
    /// <param name="i"></param>
    private void _computeSlipModelConstants(List<GameTickPhysicsData> x, int i)
    {
      if (_isComputingSlipModelConstants)
      {
        return;
      }

      _isComputingSlipModelConstants = true;

      BackgroundWorker bw = new BackgroundWorker();
      bw.DoWork += bw_DoWork;
      bw.RunWorkerCompleted += bw_RunWorkerCompleted;
      bw.RunWorkerAsync(new DataForCalculation() { X = x, I = i });

      /*
      // 2014-05-12
      double A = (-3 * e * b * q + f * b * q + 2 * e * c * q - f * c * q - 2 * c * c * q + e * d * q + 2 * b * d * q +
         3 * c * d * q - 3 * d * d * q + 3 * e * a * r - f * a * r - 2 * e * c * r + f * c * r + 2 * b * c * r -
         c * c * r - e * d * r - 2 * a * d * r - 2 * b * d * r + c * d * r + 2 * d * d * r - 2 * e * a * s +
         f * a * s + 2 * e * b * s - f * b * s - 2 * b * b * s + 2 * a * c * s + b * c * s - a * d * s +
         2 * b * d * s - 3 * c * d * s + d * d * s - e * a * t + e * b * t + 2 * b * b * t - 2 * a * c * t -
         3 * b * c * t + 3 * c * c * t + 3 * a * d * t - 2 * b * d * t - c * d * t) / (e * b * q - e * c * q +
         c * c * q - b * d * q - c * d * q + d * d * q - e * a * r + e * c * r - b * c * r + a * d * r +
         b * d * r - d * d * r + e * a * s - e * b * s + b * b * s - a * c * s - b * d * s + c * d * s -
         b * b * t + a * c * t + b * c * t - c * c * t - a * d * t + b * d * t);

      double B = (-e * q + b * q + A * b * q - 3 * c * q - 2 * A * c * q + 3 * d * q + A * d * q + e * r - a * r -
        A * a * r + 2 * b * r + A * b * r + A * c * r - 2 * d * r - A * d * r + a * s + A * a * s -
        3 * b * s - 2 * A * b * s + 3 * c * s + A * c * s - d * s) / (-b * q + c * q + a * r - c * r -
        a * s + b * s);

      double k = (a + A * a + B * a - 3 * b - 2 * A * b - B * b + 3 * c + A * c - d) / (q - r);

      double K = (-a - A * a - B * a + 2 * b + A * b - c + k * q) / k;
      */
      
      /*
      najlepsie co mam :) zakomentovane 2014-05-27 kvoli pokusom s regresiou
      double A =
        (-2 * c * c * q + 2 * b * d * q + 3 * c * d * q - 3 * d * d * q + 2 * b * c * r - c * c * r - 2 * a * d * r - 2 * b * d * r +
        c * d * r + 2 * d * d * r - 2 * b * b * s + 2 * a * c * s + b * c * s - a * d * s + 2 * b * d * s - 3 * c * d * s + d * d * s
        + 2 * b * b * t - 2 * a * c * t - 3 * b * c * t +
        3 * c * c * t + 3 * a * d * t - 2 * b * d * t - c * d * t)
        / (c * c * q - b * d * q - c * d * q + d * d * q - b * c * r + a * d * r + b * d * r -
        d * d * r + b * b * s - a * c * s - b * d * s + c * d * s - b * b * t + a * c * t + b * c * t - c * c * t - a * d * t + b * d * t);

      double B =
        (c * c * q - b * d * q - 2 * c * d * q + 3 * d * d * q - b * c * r + c * c * r +
        a * d * r + b * d * r - 2 * c * d * r - d * d * r + b * b * s - a * c * s -
        b * c * s + c * c * s + a * d * s - 2 * b * d * s + 2 * c * d * s - d * d * s - b * b * t +
        a * c * t + 2 * b * c * t - 3 * c * c * t - 2 * a * d * t + 2 * b * d * t + 2 * c * d * t -
        d * d * t)
        / (c * c * q - b * d * q - c * d * q + d * d * q - b * c * r +
        a * d * r + b * d * r - d * d * r + b * b * s - a * c * s - b * d * s +
        c * d * s - b * b * t + a * c * t + b * c * t - c * c * t - a * d * t + b * d * t);
      double k = (c * (A + B + 1) - d * (2 * A + B + 3)) / (s - t);
      double K = (c * t * (A + B + 1) - d * (s * (A + B + 1) + (A + 2) * t)) / (s - t);
      */

      /*
      if (A < 0.01 || B < 0.001 || k < 0.01 || K < 0.01)
      {
        System.Console.WriteLine("Skip slip model computation. A,B,k,K<0");
        return;
      }
      */

      /*
      this.ModelConstants.A = A;
      this.ModelConstants.B = B;
      this.ModelConstants.k = k;
      this.ModelConstants.K = K;
      this.ModelConstants.IsSlipModelComputed = true;

      Console.WriteLine(String.Format("A={0}\tB={1}", A, B));
      Console.WriteLine(String.Format("K={0}\tk={1}", K, k));
      */
    }

    void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      _isComputingSlipModelConstants = false;
    }

    void bw_DoWork(object sender, DoWorkEventArgs ea)
    {
      DataForCalculation dfc = ea.Argument as DataForCalculation;
      List<GameTickPhysicsData> x = dfc.X;
      int i = dfc.I;
      int futureTicks = 7;

      /*
      if (this.ModelConstants.IsSlipModelComputed)
      {
        if (x.Count >= 60 && Math.Abs(x[i - 0].Angle) > 10)
        {
          i = i - 10;
          futureTicks = 17;
        }
        else
        {
          return;
        }
      }
      */

        /*
      double a = Math.Abs(x[i - 4].Angle);
      double b = Math.Abs(x[i - 5].Angle);
      double c = Math.Abs(x[i - 6].Angle);
      double d = Math.Abs(x[i - 7].Angle);
      double e = Math.Abs(x[i - 8].Angle);
      double f = Math.Abs(x[i - 9].Angle);

      double q = Math.Pow(x[i - 5].Velocity, 2) / Math.Abs(x[i - 5].Radius);
      double r = Math.Pow(x[i - 6].Velocity, 2) / Math.Abs(x[i - 6].Radius);
      double s = Math.Pow(x[i - 7].Velocity, 2) / Math.Abs(x[i - 7].Radius);
      double t = Math.Pow(x[i - 8].Velocity, 2) / Math.Abs(x[i - 8].Radius);
        */

      double bestSSE = Double.PositiveInfinity;
      ModelConstants bestConstants = null;

      long start = DateTime.Now.Ticks;

      for (double A = 0.05; A < 0.2; A += 0.005)
      {
        for (double B = 0.0002; B < 0.002; B += 0.0002)
        {
          for (double K = 0.2; K <= 0.5; K += 0.02)
          {
            for (double k = 0.5; k < 0.6; k += 0.002)
            {
              double sse = 0;
              ModelConstants cons = new HWO.ModelConstants() { A = A, B = B, K = K, k = k };
              this.ModelConstants.A = A;
              this.ModelConstants.B = B;
              this.ModelConstants.k = k;
              this.ModelConstants.K = K;

              GameTickPhysicsData prev = x[i - 9];
              GameTickPhysicsData curr = x[i - 8];
              for (int j = 0; j <= futureTicks; j++)
              {
                GameTickPhysicsData future = this.CalculatePrediction(curr, prev, 1, x[i - 8 + j].Throttle, SwitchLaneData.NoChange)[0];
                sse += Math.Pow((future.Angle - x[i - 7 + j].Angle), 2);

                prev = curr;
                curr = future;
              }

              sse /= (double)(futureTicks + 1);

              if (sse < bestSSE)
              {
                bestSSE = sse;
                bestConstants = cons;
              }
            }
          }
        }
      }

      double a0 = bestConstants.A - 0.02;
      double a1 = bestConstants.A + 0.02;
      double b0 = bestConstants.B - 0.0004;
      double b1 = bestConstants.B + 0.0004;
      double K0 = bestConstants.K - 0.0;
      double K1 = bestConstants.K + 0.0;
      double k0 = bestConstants.k - 0.004;
      double k1 = bestConstants.k + 0.004;

      long finish1 = DateTime.Now.Ticks;
      double ms1 = (double)(finish1 - start) / 10000f;

      for (double A = a0; A <= a1; A += 0.0005)
      {
        for (double B = b0; B <= b1; B += 0.00005)
        {
          for (double K = K0; K <= K1; K += 0.005)
          {
            for (double k = k0; k <= k1; k += 0.0005)
            {
              double sse = 0;
              ModelConstants cons = new HWO.ModelConstants() { A = A, B = B, K = K, k = k };
              this.ModelConstants.A = A;
              this.ModelConstants.B = B;
              this.ModelConstants.k = k;
              this.ModelConstants.K = K;

              GameTickPhysicsData prev = x[i - 9];
              GameTickPhysicsData curr = x[i - 8];
              for (int j = 0; j <= futureTicks; j++)
              {
                GameTickPhysicsData future = this.CalculatePrediction(curr, prev, 1, x[i - 8 + j].Throttle, SwitchLaneData.NoChange)[0];
                sse += Math.Pow((future.Angle - x[i - 7 + j].Angle), 2);

                prev = curr;
                curr = future;
              }

              sse /= (double)(futureTicks + 1);

              if (sse < bestSSE)
              {
                bestSSE = sse;
                bestConstants = cons;
              }
            }
          }
        }
      }


      long finish2 = DateTime.Now.Ticks;
      double ms = (double)(finish2 - finish1) / 10000f;

      if (1.3 * bestSSE < this.ModelConstants.AccelerationModelSSE || !this.ModelConstants.IsSlipModelComputed)
      {
        this.ModelConstants.A = bestConstants.A;
        this.ModelConstants.B = bestConstants.B;
        this.ModelConstants.k = bestConstants.k;
        this.ModelConstants.K = bestConstants.K;
        this.ModelConstants.AccelerationModelSSE = bestSSE;
        this.ModelConstants.IsSlipModelComputed = true;

        Console.WriteLine(String.Format("Computation time: {0} + {1} ms", (int)ms1, (int)ms));
        Console.WriteLine(String.Format("A={0}\tB={1}", bestConstants.A, bestConstants.B));
        Console.WriteLine(String.Format("K={0}\tk={1}", bestConstants.K, bestConstants.k));
        Console.WriteLine(String.Format("SSE={0}", bestSSE));
      }
      else
      {
        Console.WriteLine("SSE > best...");
      }
    }


    /// <summary>
    /// Calculate total length for all pieces and all lanes in map
    /// </summary>
    private void _calculateTotalPieceLengths()
    {
      foreach (TrackPieceData piece in Track.Pieces)
      {
        piece.TotalLength = new double[Track.Lanes.Count];

        if (piece.Radius == 0)
        {
          // straight line (w/ or w/o crossing)
          for (int i = 0; i < Track.Lanes.Count; i++)
          {
            piece.TotalLength[Track.Lanes[i].Index] = piece.Length;
          }
        }
        else
        {
          // curve
          for (int i = 0; i < Track.Lanes.Count; i++)
          {
            double radius = piece.Radius;
            if (piece.Angle < 0)
              radius = -radius - Track.Lanes[i].DistanceFromCenter;
            else
              radius = radius - Track.Lanes[i].DistanceFromCenter;
            double len = 2 * Math.PI * Math.Abs(radius) * Math.Abs(piece.Angle) / 360.0;
            piece.TotalLength[Track.Lanes[i].Index] = len;
          }
        }
      }
    }


    /*
    public class PiecePositionData
    {
      public int PieceIndex;
      public double Radius;
      public int LaneIndex;
      public double InPieceDistance;
    }

    public class PredictionData
    {
      public double Throttle;
      public double Velocity;
      public double Angle;
      public PiecePositionData Position = new PiecePositionData();
    }
    */

    private bool _willCrash(GameTickPhysicsData currentPositionData, GameTickPhysicsData previousPositionData, double throttle, SwitchLaneData switchLane, int ticks)
    {
      GameTickPhysicsData curr = currentPositionData;
      GameTickPhysicsData prev = previousPositionData;
      GameTickPhysicsData next = null;


      List<GameTickPhysicsData> future = new List<GameTickPhysicsData>();
      future.Add(prev);
      future.Add(curr);
      future.AddRange(this.CalculatePrediction(curr, prev, ticks, throttle, switchLane));
      for (int i = 0; i < future.Count; i++)
      {
        if (Math.Abs(future[i].Angle) >= this.ModelConstants.MaxSlipAngle) return true;

        if (Math.Abs(future[i].Radius) <= 60)
        {
          // lower the slip limit on fast accelerating angle
//          if (Math.Abs(future[i].Angle) >= 0.75 * this.ModelConstants.MaxSlipAngle) return true;
        }
      }

      curr = future[future.Count - 1];
      prev = future[future.Count - 2];


      future = this.CalculatePrediction(curr, prev, 200, 0, switchLane);
      for (int i = 0; i < future.Count; i++)
      {
        if (Math.Abs(future[i].Angle) >= this.ModelConstants.MaxSlipAngle) return true;

        if (Math.Abs(future[i].Radius) <= 60)
        {
          // lower the slip limit on fast accelerating angle
//          if (Math.Abs(future[i].Angle) >= 0.75 * this.ModelConstants.MaxSlipAngle) return true;
        }
      }
      
      

      /*
      // compute if crash on maximum
      for (int i = 0; i < ticks; i++)
      {
        next = this.CalculatePrediction(curr, prev, 1, throttle, switchLane)[0];
        // crash
        if (Math.Abs(next.Angle) >= this.ModelConstants.MaxSlipAngle)
        {
          return true;
        }
        // move pivot
        prev = curr;
        curr = next;
      }

      for (int i = 0; i < 200; i++)
      {
        next = this.CalculatePrediction(curr, prev, 1, 0, switchLane)[0];
        // crash
        if (Math.Abs(next.Angle) >= this.ModelConstants.MaxSlipAngle)
        {
          return true;
        }
        // move pivot
        prev = curr;
        curr = next;
      }
      */

      return false;
    }

    public int HowLongCanThrottle(GameTickPhysicsData currentPositionData, GameTickPhysicsData previousPositionData, double throttle, SwitchLaneData switchLane, int maxTicks)
    {
      // compute if crash on maximum
      for (int i = 0; i < maxTicks; i++)
      {
        if (_willCrash(currentPositionData, previousPositionData, throttle, switchLane, i))
        {
          return i;
        }
      }
      return maxTicks;
    }

    public int HowLongCanThrottle(double throttle, SwitchLaneData switchLane, int maxTicks)
    {
      GameTickPhysicsData currentPositionData = Data[_myCarName][Data[_myCarName].Count - 1];
      GameTickPhysicsData previousPositionData = Data[_myCarName][Data[_myCarName].Count - 2];
      return HowLongCanThrottle(currentPositionData, previousPositionData, throttle, switchLane, maxTicks);
    }



    public List<GameTickPhysicsData> CalculatePrediction(int futureTicks, double throttle, SwitchLaneData switchLane)
    {
      GameTickPhysicsData currentPositionData = Data[_myCarName][Data[_myCarName].Count - 1];
      GameTickPhysicsData previousPositionData = Data[_myCarName][Data[_myCarName].Count - 2];
      return CalculatePrediction(currentPositionData, previousPositionData, futureTicks, throttle, switchLane);
    }

    public List<GameTickPhysicsData> CalculatePrediction(
      GameTickPhysicsData currentPositionData,
      GameTickPhysicsData previousPositionData,
      int futureTicks,
      double throttle,
      SwitchLaneData switchLane)
    {
      // add current and previous data at start of prediction
      List<GameTickPhysicsData> prediction = new List<GameTickPhysicsData>();
      prediction.Add(previousPositionData);
      prediction.Add(currentPositionData);

      // calculate next n ticks
      for (int i = 0; i < futureTicks; i++)
      {
        GameTickPhysicsData previous = prediction[i];
        GameTickPhysicsData current = prediction[i + 1];
        GameTickPhysicsData next = new GameTickPhysicsData();

        next.Throttle = throttle;

        // velocity
        next.Velocity = this.ModelConstants.F * throttle - this.ModelConstants.f * current.Velocity + current.Velocity;

        // total length of current piece
        double totalLength = (Track.Pieces[current.PiecePosition.PieceIndex].TotalLength[current.PiecePosition.Lane.StartLaneIndex]
          + Track.Pieces[current.PiecePosition.PieceIndex].TotalLength[current.PiecePosition.Lane.EndLaneIndex]) / 2.0;

        // piece position
        next.PiecePosition.InPieceDistance = current.PiecePosition.InPieceDistance + next.Velocity;
        next.PiecePosition.Lap = current.PiecePosition.Lap;
        next.PiecePosition.Lane.StartLaneIndex = current.PiecePosition.Lane.StartLaneIndex;
        next.PiecePosition.Lane.EndLaneIndex = current.PiecePosition.Lane.EndLaneIndex;
        next.PiecePosition.PieceIndex = current.PiecePosition.PieceIndex;
        // next piece
        if (next.PiecePosition.InPieceDistance >= totalLength)
        {
          // distance
          next.PiecePosition.InPieceDistance = next.PiecePosition.InPieceDistance - totalLength;
          // index
          next.PiecePosition.PieceIndex++;
          // new lap
          if (next.PiecePosition.PieceIndex >= Track.Pieces.Count)
          {
            next.PiecePosition.PieceIndex = 0;
            next.PiecePosition.Lap++;
          }
          // lane
          next.PiecePosition.Lane.StartLaneIndex = current.PiecePosition.Lane.EndLaneIndex;
          // check if next piece is crossing
          TrackPieceData nextPiece = Track.Pieces[next.PiecePosition.PieceIndex];
          if (nextPiece.Switch)
          {
            int endLane = next.PiecePosition.Lane.StartLaneIndex;
            if (switchLane == SwitchLaneData.Left && endLane > 0) endLane--;
            if (switchLane == SwitchLaneData.Right && endLane < Track.Lanes.Count - 1) endLane++;
            next.PiecePosition.Lane.EndLaneIndex = endLane;
          }
        }

        // total length of next piece
        double nextPieceTotalLength = (Track.Pieces[next.PiecePosition.PieceIndex].TotalLength[next.PiecePosition.Lane.StartLaneIndex]
          + Track.Pieces[next.PiecePosition.PieceIndex].TotalLength[next.PiecePosition.Lane.EndLaneIndex]) / 2.0;

        // lane
        TrackPieceData pieceData = Track.Pieces[next.PiecePosition.PieceIndex];
        int lane;
        if (next.PiecePosition.InPieceDistance < nextPieceTotalLength / 2)
          lane = next.PiecePosition.Lane.StartLaneIndex;
        else
          lane = next.PiecePosition.Lane.EndLaneIndex;

        // radius
        double radius = pieceData.Radius;
        if (radius != 0)
        {
          if (pieceData.Angle < 0)
            radius = -radius - Track.Lanes[lane].DistanceFromCenter;
          else
            radius = radius - Track.Lanes[lane].DistanceFromCenter;
        }
        next.Radius = radius;

        // angle
        /*
        double v2r = (current.Radius == 0) ? 0 : Math.Pow(current.Velocity, 2) / current.Radius;
        double v2rk = 0;
        if (v2r > this.ModelConstants.K)
          v2rk = v2r - this.ModelConstants.K;
        else if (v2r < -this.ModelConstants.K)
          v2rk = v2r + this.ModelConstants.K;
        next.Angle = (v2rk * ModelConstants.k + current.Angle * (2 + ModelConstants.A - ModelConstants.B * current.Velocity) - previous.Angle) / (1 + ModelConstants.A);
        */

        // seilgu
        double v2r = (current.Radius == 0) ? 0 : Math.Pow(current.Velocity, 2) / Math.Sqrt(Math.Abs(current.Radius));
        double v2rk = 0;
        if (v2r * this.ModelConstants.k > current.Velocity * this.ModelConstants.K)
        {
          v2rk = v2r * this.ModelConstants.k - current.Velocity * this.ModelConstants.K;
          v2rk *= Math.Sign(current.Radius);
        }

        next.Angle = (v2rk + current.Angle * (2 + ModelConstants.A) - previous.Angle) / (1 + ModelConstants.A + ModelConstants.B * current.Velocity);



        /*
        if (v2r <= (this.ModelConstants.K / this.ModelConstants.k))
        {
          next.Angle = ((this.ModelConstants.A + 2) * current.Angle - previous.Angle)
            / (1 + this.ModelConstants.A + this.ModelConstants.B);
        }
        else
        {
          double x = Math.Sign(current.Radius) * (v2r * this.ModelConstants.k - this.ModelConstants.K);
          next.Angle = (x + (this.ModelConstants.A + 2) * current.Angle - previous.Angle)
            / (1 + this.ModelConstants.A + this.ModelConstants.B);
        }
        */
 
        // add prediction to list
        prediction.Add(next);
      }
      
      // clean list
      prediction.RemoveRange(0, 2);

      return prediction;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="carPositions"></param>
    /// <param name="gameTick"></param>
    public void AddTickData(List<CarPositionData> carPositions, int gameTick)
    {
      foreach (CarPositionData pos in carPositions)
      {
        // first-time create list
        if (!Data.ContainsKey(pos.Id.Name)) Data.Add(pos.Id.Name, new List<GameTickPhysicsData>());
        // get list
        List<GameTickPhysicsData> list = Data[pos.Id.Name];
        // create current data record
        GameTickPhysicsData data = new GameTickPhysicsData();
        // store current record
        list.Add(data);

        // set new record values
        data.GameTick = gameTick;
        data.PiecePosition = pos.PiecePosition;
        
        // current index
        int i = Data[pos.Id.Name].Count - 1;


        // RADIUS
        // current piece radius
        double r = Track.Pieces[pos.PiecePosition.PieceIndex].Radius;
        if (r == 0)
          data.Radius = 0;
        else
        {
          // adjust by current lane radius
          int lane = pos.PiecePosition.Lane.StartLaneIndex;
          // anjust lane on crossing after half piece length
          if (pos.PiecePosition.Lane.StartLaneIndex != pos.PiecePosition.Lane.EndLaneIndex)
          {
            // TODO
          }

          // adjust on left/right curve
          if (Track.Pieces[pos.PiecePosition.PieceIndex].Angle < 0)
          {
            data.Radius = -r - Track.Lanes[lane].DistanceFromCenter;
          }
          else
          {
            data.Radius = r - Track.Lanes[lane].DistanceFromCenter;
          }
        }


        // VELOCITY
        double v = 0;
        if (i > 0)
        {
          v = list[i].PiecePosition.InPieceDistance - list[i - 1].PiecePosition.InPieceDistance;
          if (v < 0)
          {
            // compute velocity with length substraction
            v = v + (Track.Pieces[list[i - 1].PiecePosition.PieceIndex].TotalLength[list[i - 1].PiecePosition.Lane.StartLaneIndex]
              + Track.Pieces[list[i - 1].PiecePosition.PieceIndex].TotalLength[list[i - 1].PiecePosition.Lane.EndLaneIndex]) / 2;
            // compute velocity by our model
            double vpre = list[i - 1].Velocity + this.ModelConstants.F * list[i - 1].Throttle - this.ModelConstants.f * list[i - 1].Velocity;
// !!!!!!
            v = vpre;
          }
        }
        data.Velocity = v;


        // ANGLE
        data.Angle = pos.Angle;


        // LOG
        //System.IO.File.AppendAllText(@"C:\temp\hworace.txt", String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\r\n", data.GameTick, data.Angle, data.PiecePosition.Lap, data.PiecePosition.PieceIndex, data.PiecePosition.InPieceDistance, data.Radius, data.Velocity, data.Throttle));


        // == COMPUTE VARS ==

        //if (pos.Id.Name == _myCarName)
        {
          // compute target velocities at curves while evaluating slip model
          if (gameTick == 0)
          {
            this.CalculateTargetConstantVelocitiesWhileEvaluatingSlipModel(pos.PiecePosition.Lane.StartLaneIndex);
          }

          // Ff Calculation
          if (!this.ModelConstants.IsAccelerationModelComputed
            && i >= 2
            && list[i - 2].Throttle > 0
            && list[i - 1].Velocity > 0)
          {
            _computeConstantsFf(list, i);
          }

          // AB+Kk Calculation
          if (/*!this.ModelConstants.IsSlipModelComputed
            && */i >= 9
            && list[i - 5].Radius != 0
            && list[i - 6].Radius != 0
            && list[i - 7].Radius != 0
            && list[i - 8].Radius != 0
            && list[i - 9].Angle != 0
            && Math.Abs(list[i - 9].Angle) >= 1
            && Math.Abs(list[i - 4].Angle) >= 1
            && Math.Abs(list[i - 4].Angle) > Math.Abs(list[i - 9].Angle)
            && list[i - 5].Velocity > 0
            && list[i - 1].Velocity > 0
            && list[i - 0].Velocity > 0
            )
          {
            // compute AB constants
            _computeSlipModelConstants(list, i);
          }
          
        }
 
      }
    }

  }
}
