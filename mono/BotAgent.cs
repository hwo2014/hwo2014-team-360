using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using HWO.MessageData;
using HWO.Messages;
using Newtonsoft.Json;

namespace HWO
{
  public class BotAgent
  {
    private StreamReader _networkReader;
    private StreamWriter _networkWriter;

    PhysicsSimulator _simulator;
    CarIdData _myCar;


    public BotAgent(StreamReader reader, StreamWriter writer, string botName, string botKey)
    {
      this._networkWriter = writer;
      this._networkReader = reader;

      // init simulator
      _simulator = new PhysicsSimulator(botName);

      // join race
      _sendMessage(new JoinClientMessage(botName, botKey));
      //_sendMessage(new JoinRaceClientMessage(botName, botKey, "elaeintarha", "123", 1));
      //_sendMessage(new JoinRaceClientMessage(botName, botKey, "imola", null, 1));
    }

    private double _availableTurbo = 1;
    private int _availableTurboDuration = 0;
    private double _currentTurbo = 1;

    private bool _isCrashed = false;
    private int _crashesCount = 0;


    /// <summary>
    /// 
    /// </summary>
    public void MainLoop()
    {
      string line;

      //File.WriteAllText(@"C:\temp\hworace.txt", String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\r\n", "GameTick", "Angle", "Lap", "PieceIndex", "InPieceDistance", "Radius", "Velocity", "Throttle", "NextAlfaPrediction"));
      //File.WriteAllText(@"C:\temp\hwodump.txt", "");

      // we are using this to adjust crash angle limit on the fly to fit prediction error
      GameTickPhysicsData prediction = null;
      double predictionHighestError = 1.0;

      // loop of messages
      while ((line = _networkReader.ReadLine()) != null)
      {
        // deserialize message
        MessageWrapper msg = JsonConvert.DeserializeObject<MessageWrapper>(line);

        //Console.WriteLine(msg.MsgType + "\t" + msg.Data);
        //File.AppendAllText(@"C:\temp\hwodump.txt", JsonConvert.SerializeObject(JsonConvert.DeserializeObject(line), Formatting.Indented) + "\r\n");

        // process message
        switch (msg.MsgType)
        {
          case "carPositions":
            CarPositionsServerMessage pos = JsonConvert.DeserializeObject<CarPositionsServerMessage>(line);
            
            // feed our simulator with new data
            _simulator.AddTickData(pos.Data, pos.GameTick);

            // evaluate last prediction for error
            if (prediction != null && prediction.Angle != 0 && _simulator.ModelConstants.IsSlipModelComputed)
            {
              double err = Math.Abs(_simulator.MyLastTickData.Angle) / Math.Abs(prediction.Angle);
              if (err > predictionHighestError && Math.Abs(_simulator.MyLastTickData.Angle) > 5)
              {
                predictionHighestError = err;
                _simulator.ModelConstants.MaxSlipAngle = _simulator.ModelConstants.MaxSlipAngle / predictionHighestError;
                Console.WriteLine("Slip angle limit adjusted: " + _simulator.ModelConstants.MaxSlipAngle.ToString());
              }

              if (_simulator.ModelConstants.MaxSlipAngle < 45)
              {
                Console.WriteLine("Slip limit <45, resetting constants...");
                _simulator.ModelConstants.IsSlipModelComputed = false;
                _simulator.ModelConstants.MaxSlipAngle = 58;
              }
            }

            // === compute switching ===
            // if next piece will be crossing..
            int nextPieceIndex = (_simulator.MyLastTickData.PiecePosition.PieceIndex + 1) % _simulator.Track.Pieces.Count;
            int previousPieceIndex = (_simulator.MyLastTickData.PiecePosition.PieceIndex + _simulator.Track.Pieces.Count - 1) % _simulator.Track.Pieces.Count;
            if (_simulator.ModelConstants.IsSlipModelComputed)
            {
              for (int p = 1; p < 5; p++)
              {
                int pivot = (_simulator.MyLastTickData.PiecePosition.PieceIndex + p) % _simulator.Track.Pieces.Count;

                if (_simulator.Track.Pieces[pivot].Switch && !_simulator.Track.Pieces[pivot].SwitchEvaluated)
                {
                  // count lane lengths up to next switch
                  double[] lens = new double[_simulator.Track.Lanes.Count];

                  for (int i = 1; i < _simulator.Track.Pieces.Count; i++)
                  {
                    int pivotIndex = (pivot + i) % _simulator.Track.Pieces.Count;
                    // break when we reach next switch
                    if (_simulator.Track.Pieces[pivotIndex].Switch) break;
                    // count lengths
                    for (int j = 0; j < _simulator.Track.Lanes.Count; j++)
                    {
                      lens[_simulator.Track.Lanes[j].Index] += _simulator.Track.Pieces[pivotIndex].TotalLength[j];
                    }
                  }

                  // evaluate shortest path
                  double leftPathLength = Double.MaxValue;
                  double currentPathLength = Double.MaxValue;
                  double rightPathLength = Double.MaxValue;
                  int currentLane = _simulator.MyLastTickData.PiecePosition.Lane.EndLaneIndex;
                  currentPathLength = lens[_simulator.Track.Lanes[currentLane].Index];
                  if (currentLane > 0) leftPathLength = lens[_simulator.Track.Lanes[currentLane - 1].Index];
                  if (currentLane < _simulator.Track.Lanes.Count - 1) rightPathLength = lens[_simulator.Track.Lanes[currentLane + 1].Index];

                  // set request
                  if (currentPathLength <= leftPathLength && currentPathLength <= rightPathLength)
                  {
                    _simulator.Track.Pieces[pivot].SwitchRequest = SwitchLaneData.NoChange;
                  }
                  else if (leftPathLength < rightPathLength)
                  {
                    _simulator.Track.Pieces[pivot].SwitchRequest = SwitchLaneData.Left;
                  }
                  else
                  {
                    _simulator.Track.Pieces[pivot].SwitchRequest = SwitchLaneData.Right;
                  }

                  // flag
                  _simulator.Track.Pieces[pivot].SwitchEvaluated = true;

                  break;
                }
              }
            }

            // if previous piece was switch, clear 'evaluated' flag
            if (_simulator.Track.Pieces[previousPieceIndex].Switch)
            {
              _simulator.Track.Pieces[previousPieceIndex].SwitchEvaluated = false;
              _simulator.Track.Pieces[previousPieceIndex].SwitchRequest = SwitchLaneData.NoChange;
            }

            // === compute next throttle ===
            double nextThrottle = 1.0;

            // while we don't have slip model constants we have to get into drift
            if (!_simulator.ModelConstants.IsSlipModelComputed && _simulator.MyLastTickData != null && _simulator.MyPreviousTickData != null && _simulator.ModelConstants.IsAccelerationModelComputed)
            {
              // find out nearest target velocity
              double nearestTargetVelocity = 0;
              // while we don't have AB constants, run at constant velocities at curves
              for (int i = 0; i < _simulator.Track.Pieces.Count; i++)
              {
                TrackPieceData pivot = _simulator.Track.Pieces[(_simulator.MyLastTickData.PiecePosition.PieceIndex + i) % _simulator.Track.Pieces.Count];
                if (pivot.TargetVelocityWhileEvaluating != 0)
                {
                  nearestTargetVelocity = pivot.TargetVelocityWhileEvaluating;
                  break;
                }
              }
              // set throttle to reach it
              nextThrottle = (nearestTargetVelocity - _simulator.MyLastTickData.Velocity + _simulator.ModelConstants.f * _simulator.MyLastTickData.Velocity) / _simulator.ModelConstants.F;

              // speed up at first tick of curve
              if (_simulator.MyLastTickData.Radius != 0 && _simulator.MyPreviousTickData.Radius == 0)
              {
                nextThrottle = nextThrottle + 0.2;
              }

              if (nextThrottle > 1) nextThrottle = 1;
              if (nextThrottle < 0) nextThrottle = 0;
            }

            // we have slip model, so we can compute throttle with slip angle prediction
            if (_simulator.ModelConstants.IsSlipModelComputed)
            {
              long start = DateTime.Now.Ticks;

              // try to find nearest change request
              // also look for tightest curve
              SwitchLaneData sw = SwitchLaneData.NoChange;
              double tightestCurve = Double.PositiveInfinity;
              double thresholdMultiplyier = 1;
              for (int i = 0; i < 5; i++)
              {
                int pivotIndex = (nextPieceIndex + i) % _simulator.Track.Pieces.Count;
                // evaluate only nearest switch
                if (sw == SwitchLaneData.NoChange && _simulator.Track.Pieces[pivotIndex].Switch && _simulator.Track.Pieces[pivotIndex].SwitchEvaluated)
                {
                  sw = _simulator.Track.Pieces[pivotIndex].SwitchRequest;
                }

                double r = Math.Abs(_simulator.Track.Pieces[pivotIndex].Radius);
                if (i < 1 && r <= 60 && r != 0)
                {
                  thresholdMultiplyier = 3;
                }
                if (i < 5 && r <= 60 && r != 0 && thresholdMultiplyier == 1)
                {
                  thresholdMultiplyier = 2;
                }
              }

              // if we can set full throttle, set it, else set to zero
              nextThrottle = 0;
              int maximumDuration = _simulator.HowLongCanThrottle(1 * _currentTurbo, sw, 10);

              //Console.WriteLine(String.Format("Tick {0}, can throttle {1}", pos.GameTick, maximumDuration));

              if (maximumDuration > 4 * thresholdMultiplyier)
                nextThrottle = 1;
              else if (maximumDuration > 3 * thresholdMultiplyier)
                nextThrottle = 0.5;
              else if (maximumDuration > 2 * thresholdMultiplyier)
                nextThrottle = 0.1;
              else
                nextThrottle = 0.0;

              // if theres very tight curve in front of us
              //nextThrottle *= throttleMultiplyier;

              long finish = DateTime.Now.Ticks;
              double ms = ((double)finish - (double)start) / 10000.0;
              if (ms > 20) Console.WriteLine(String.Format("!!! Time: {0} ms", ms));
            }

            bool startTurbo = false;
            
            // start turbo on straight line
            if (_availableTurbo != 1)
            {
              // try to find nearest change request
              SwitchLaneData sw = SwitchLaneData.NoChange;
              for (int i = 0; i < 10; i++)
              {
                int pivotIndex = (nextPieceIndex + i) % _simulator.Track.Pieces.Count;
                // break when we reach next switch
                if (_simulator.Track.Pieces[pivotIndex].Switch && _simulator.Track.Pieces[pivotIndex].SwitchEvaluated)
                {
                  sw = _simulator.Track.Pieces[pivotIndex].SwitchRequest;
                  break;
                }
              }

              int maximumDuration = _simulator.HowLongCanThrottle(1 * _availableTurbo, sw, 20);

              if (maximumDuration > 15 && _simulator.MyLastTickData.Velocity > 0)
              {
                startTurbo = true;
              }

              if (!startTurbo)
              {
                if (_simulator.MyLastTickData.PiecePosition.PieceIndex == _simulator.LongestStraightStartIndex
                  || _simulator.MyLastTickData.PiecePosition.PieceIndex == _simulator.SecondLongestStraightStartIndex)
                {
                  startTurbo = true;
                }
              }
            }

            // check if some cars are not in front of us
            foreach (CarData car in _simulator.Cars)
            {
              if (car.Id.Name == this._myCar.Name && car.Id.Color == this._myCar.Color) continue;
              GameTickPhysicsData carData = _simulator.GetLastTickData(car.Id.Name);

              // same lane
              if (carData.PiecePosition.Lane.EndLaneIndex != _simulator.MyLastTickData.PiecePosition.Lane.EndLaneIndex) continue;

              // check distance in front of us
              int dist = (_simulator.Track.Pieces.Count + carData.PiecePosition.PieceIndex - _simulator.MyLastTickData.PiecePosition.PieceIndex) % _simulator.Track.Pieces.Count;
              if (dist <= 2)
              {
                // same lap (don't pew the slow ones, override them instead
                if (carData.PiecePosition.Lap >= _simulator.MyLastTickData.PiecePosition.Lap)
                {
                  if (_availableTurbo != 1 && Math.Abs(carData.Angle) > 20)
                  {
                    startTurbo = true;
                    break;
                  }
                }
              }
              if (dist < 1)
              {
                //if (!startTurbo)
                {
                  if (_simulator.GetLastTickData(car.Id.Name).Velocity < _simulator.MyLastTickData.Velocity)
                  {
                    int ourIndex = _simulator.MyLastTickData.PiecePosition.PieceIndex;
                    for (int i = ourIndex + 1; i < ourIndex + _simulator.Track.Pieces.Count; i++)
                    {
                      int pivot = i % _simulator.Track.Pieces.Count;
                      if (_simulator.Track.Pieces[pivot].Switch && !_simulator.Track.Pieces[pivot].SwitchEvaluated)
                      {
                        int ourLane = _simulator.MyLastTickData.PiecePosition.Lane.EndLaneIndex;

                        if (ourLane < _simulator.Track.Lanes.Count - 1)
                        {
                          _simulator.Track.Pieces[pivot].SwitchRequest = SwitchLaneData.Right;
                        }
                        else if (ourLane > 0)
                        {
                          _simulator.Track.Pieces[pivot].SwitchRequest = SwitchLaneData.Left;
                        }

                        _simulator.Track.Pieces[pivot].SwitchEvaluated = true;
                        break;
                      }

                    }
                  }
                }

              }
            }

            
            if (_simulator.MyPreviousTickData != null) _simulator.MyLastTickData.Throttle = _simulator.MyPreviousTickData.Throttle;

            //
            if (startTurbo)
            {
              _sendMessage(new TurboClientMessage("Pew!", pos.GameTick));
            }
            else if (_simulator.Track.Pieces[nextPieceIndex].SwitchEvaluated && _simulator.Track.Pieces[nextPieceIndex].SwitchRequest != SwitchLaneData.NoChange)
            {
              // send switch message
              _sendMessage(new SwitchLaneClientMessage(_simulator.Track.Pieces[nextPieceIndex].SwitchRequest, pos.GameTick));
              // "clear flag"
              _simulator.Track.Pieces[nextPieceIndex].SwitchRequest = SwitchLaneData.NoChange;
            }
            else
            {
              _sendMessage(new ThrottleClientMessage(nextThrottle, pos.GameTick));
              // store current throttle
              _simulator.MyLastTickData.Throttle = _currentTurbo * nextThrottle;
            }


            // predict next tick
            if (_simulator.ModelConstants.IsSlipModelComputed)
            {
              prediction = _simulator.CalculatePrediction(1, _simulator.MyLastTickData.Throttle, _simulator.Track.Pieces[nextPieceIndex].SwitchRequest)[0];
            }


            /*
            System.IO.File.AppendAllText(@"C:\temp\hworace.txt", String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\r\n",
              _simulator.MyLastTickData.GameTick,
              _simulator.MyLastTickData.Angle,
              _simulator.MyLastTickData.PiecePosition.Lap,
              _simulator.MyLastTickData.PiecePosition.PieceIndex,
              _simulator.MyLastTickData.PiecePosition.InPieceDistance,
              _simulator.MyLastTickData.Radius,
              _simulator.MyLastTickData.Velocity,
              _simulator.MyLastTickData.Throttle,
              (prediction == null) ? "?" : prediction.Angle.ToString()));
            */

            if (pos.GameTick % 100 == 0) Console.WriteLine("Tick: " + pos.GameTick.ToString());

            break;
          case "gameInit":
            Console.WriteLine("Game init");
            GameInitServerMessage gameInit = JsonConvert.DeserializeObject<GameInitServerMessage>(line);

            // init simulator
            _simulator.GameInit(gameInit.Data);

            break;
          case "yourCar":
            YourCarServerMessage yourCar = JsonConvert.DeserializeObject<YourCarServerMessage>(line);
            _myCar = yourCar.Data;
            break;
          case "join":
            Console.WriteLine("Joined");
            break;
          case "gameStart":
            Console.WriteLine("Race starts");
            break;
          case "gameEnd":
            Console.WriteLine("Race ended");
            break;
          case "crash":
            CrashServerMessage crash = JsonConvert.DeserializeObject<CrashServerMessage>(line);
            if (crash.Data.Name == _myCar.Name && crash.Data.Color == _myCar.Color)
            {
              Console.WriteLine("Crash!");
              _isCrashed = true;
              _crashesCount++;
              // decrease critical slip angle
              if (_crashesCount >= 1)
              {
                _simulator.ModelConstants.MaxSlipAngle *= 0.9;
              }
              if (_crashesCount % 1 == 0)
              {
                _simulator.ModelConstants.IsSlipModelComputed = false;
              }
              Console.WriteLine("Slip angle limit adjusted: " + _simulator.ModelConstants.MaxSlipAngle.ToString());
            }
            break;
          case "spawn":
            SpawnServerMessage spawn = JsonConvert.DeserializeObject<SpawnServerMessage>(line);
            if (spawn.Data.Name == _myCar.Name && spawn.Data.Color == _myCar.Color)
            {
              _isCrashed = false;
            }
            break;
          case "turboAvailable":
            TurboAvailableServerMessage turboAvailable = JsonConvert.DeserializeObject<TurboAvailableServerMessage>(line);
            if (!_isCrashed)
            {
              _availableTurbo = turboAvailable.Data.TurboFactor;
              _availableTurboDuration = turboAvailable.Data.TurboDurationTicks;
            }
            else
            {
              /* fuck */
            }
            break;
          case "turboStart":
            TurboStartServerMessage turboStart = JsonConvert.DeserializeObject<TurboStartServerMessage>(line);
            if (turboStart.Data.Name == _myCar.Name && turboStart.Data.Color == _myCar.Color)
            {
              _currentTurbo = _availableTurbo;
              _availableTurbo = 1;
            }
            else
            {
              // someone has started turbo
              GameTickPhysicsData turboman = _simulator.GetLastTickData(turboStart.Data.Name);
              // same lane
              if (turboman.PiecePosition.Lane.EndLaneIndex == _simulator.MyLastTickData.PiecePosition.Lane.EndLaneIndex)
              {
                // check distance behind us
                int dist = (_simulator.Track.Pieces.Count + _simulator.MyLastTickData.PiecePosition.PieceIndex - turboman.PiecePosition.PieceIndex) % _simulator.Track.Pieces.Count;
                if (dist <= 3)
                {
                  // we don't have turbo active
                  if (_currentTurbo == 1)
                  {
                    // find nearest crossing and request switch
                    int ourIndex = _simulator.MyLastTickData.PiecePosition.PieceIndex;
                    for (int i = ourIndex + 1; i < ourIndex + 10; i++)
                    {
                      int pivot = i % _simulator.Track.Pieces.Count;
                      if (_simulator.Track.Pieces[pivot].Switch)
                      {
                        int ourLane = _simulator.MyLastTickData.PiecePosition.Lane.EndLaneIndex;
                        if (ourLane < _simulator.Track.Lanes.Count - 1)
                        {
                          _simulator.Track.Pieces[pivot].SwitchRequest = SwitchLaneData.Right;
                        }
                        else if (ourLane > 0)
                        {
                          _simulator.Track.Pieces[pivot].SwitchRequest = SwitchLaneData.Left;
                        }
                        _simulator.Track.Pieces[pivot].SwitchEvaluated = true;
                        break;
                      }
                    }
                  }
                }
              }
            }
            break;
          case "turboEnd":
            TurboEndServerMessage turboEnd = JsonConvert.DeserializeObject<TurboEndServerMessage>(line);
            if (turboEnd.Data.Name == _myCar.Name && turboEnd.Data.Color == _myCar.Color)
            {
              _currentTurbo = 1;
              _availableTurboDuration = 0;
            }
            break;
          case "dnf":
            DnfServerMessage dnf = JsonConvert.DeserializeObject<DnfServerMessage>(line);
            Console.WriteLine(String.Format("{0} got DNF. Reason: {1}", dnf.Data.Car.Name, dnf.Data.Reason));
            break;
          default:
            break;
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    private void _sendMessage(object msg)
    {
      string serialized = JsonConvert.SerializeObject(msg);
      //File.AppendAllText(@"C:\temp\hwodump.txt", JsonConvert.SerializeObject(msg, Formatting.Indented) + "\r\n");
      _networkWriter.WriteLine(serialized);
    }

  }

}
